﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using MongoDB.Driver;
using OTT.Infrastructure.Db;

namespace OTT.Infrastructure.MongoDb
{
    public class MongoDbAdapter : IOttDb
    {
        private readonly string _connectionString;

        private readonly string _databaseName;

        public MongoDbAdapter(string databaseName, string connectionString = "")
        {
            _databaseName = databaseName;
            _connectionString = connectionString;
        }

        private IMongoClient Client
            => string.IsNullOrWhiteSpace(_connectionString)
                ? new MongoClient()
                : new MongoClient(_connectionString);

        private static string GetDocumentName<T>() where T : class => typeof(T).FullName;


        private IMongoCollection<T> GetCollection<T>() where T : class
            => Client.GetDatabase(_databaseName).GetCollection<T>(GetDocumentName<T>());

        public void Insert<T>(T log) where T : class
            => GetCollection<T>().InsertOne(log);

        private static Expression<Func<T, bool>> GetDefaultFilter<T>() where T : class => _ => true;

        public List<T> Find<T>(Expression<Func<T, bool>> filter = null) where T : class
            => GetCollection<T>().Find(filter ?? GetDefaultFilter<T>()).ToList();

        public void DeleteMany<T>(Expression<Func<T, bool>> filter = null) where T : class
            => GetCollection<T>().DeleteMany(filter ?? GetDefaultFilter<T>());

        
    }
}
