using OTT.SharedKernel;

namespace OTT.PathCalculator.Dijkstra
{
    public class AdjacentNode : ValueObject<AdjacentNode>
    {
        public Node Node { get; }

        public decimal Weight { get; }

        public AdjacentNode(Node node, decimal weight = 1)
        {
            Node = node;
            Weight = weight;
        }
    }
}