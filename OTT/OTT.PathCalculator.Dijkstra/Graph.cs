﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using OTT.SharedKernel;

namespace OTT.PathCalculator.Dijkstra
{
    public class Graph : Entity<Guid>
    {
        public Graph(Guid id) : base(id)
        {
            Nodes = new ConcurrentDictionary<string, Node>();
        }

        public Graph() : this(Guid.NewGuid())
        {
        }

        public ConcurrentDictionary<string, Node> Nodes { get; protected set; }

        public void AddNode(Node node)
        {
            Nodes.TryAdd(node.Id, node);
        }

        private Node GetNode(string nodeId)
        {
            Node node;
            if (!Nodes.TryGetValue(nodeId, out node))
            {
                throw new ArgumentOutOfRangeException("nodeId");
            }
            return node;
        }

        public List<Node> CalculateShortestPath(string srcNodeId, string dstNodeId)
        {
            if (srcNodeId == dstNodeId)
            {
                return (List<Node>)Enumerable.Empty<Node>();
            }

            var srcNode = GetNode(srcNodeId);
            srcNode.DistanceFromSrcNode = 0;

            var dstNode = GetNode(dstNodeId);

            TraverseGraph(srcNode);

            if (!dstNode.Visited)
            {
                throw new Exception("there is no path");
            }

            return RetrivePath(dstNode);
        }

        private static void TraverseGraph(Node srcNode)
        {
            var processingNode = srcNode;
            var queque = new Queue<Node>();

            while (true)
            {
                processingNode.Visited = true;

                foreach (var adjacentNode in processingNode.AdjacentNodes.OrderBy(p => p.Weight))
                {
                    var distance = processingNode.DistanceFromSrcNode + adjacentNode.Weight;
                    if (adjacentNode.Node.DistanceFromSrcNode > distance)
                    {
                        adjacentNode.Node.DistanceFromSrcNode = distance;
                    }

                    if (!adjacentNode.Node.Visited)
                    {
                        queque.Enqueue(adjacentNode.Node);
                    }
                }

                if (!queque.Any())
                {
                    break;
                }
                processingNode = queque.Dequeue();
            }
        }

        private static List<Node> RetrivePath(Node dstNode)
        {
            var processingNode = dstNode;
            var queque = new Queue<Node>();

            while (true)
            {
                queque.Enqueue(processingNode);
                var nextNode = processingNode.AdjacentNodes.FirstOrDefault(p => p.Node.DistanceFromSrcNode == processingNode.DistanceFromSrcNode - p.Weight)?.Node;

                // ReSharper disable once PossibleUnintendedReferenceComparison
                if (nextNode == default(Node))
                {
                    break;
                }
                processingNode = nextNode;
            }

            return queque.ToList();
        }
    }
}
