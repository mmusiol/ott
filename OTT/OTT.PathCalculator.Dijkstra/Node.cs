﻿using System.Collections.Concurrent;
using System.Linq;
using OTT.SharedKernel;

namespace OTT.PathCalculator.Dijkstra
{
    public class Node : Entity<string>
    {
        public Node(string id) : base(id)
        {
            AdjacentNodes = new ConcurrentBag<AdjacentNode>();
        }

        public string Label { get; set; }

        public ConcurrentBag<AdjacentNode> AdjacentNodes { get; protected set; }

        public decimal DistanceFromSrcNode { get; set; } = decimal.MaxValue;

        public bool Visited { get; set; } = false;

        public void AddAdjacentNode(Node node)
        {
            var adjacentNode = new AdjacentNode(node);
            if (!AdjacentNodes.Contains(adjacentNode))
            {
                AdjacentNodes.Add(adjacentNode);
            }
        }
    }
}