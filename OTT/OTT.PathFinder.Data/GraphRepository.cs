﻿using System;
using System.Linq;
using OTT.Infrastructure.Db;

namespace OTT.PathFinder.Data
{
    public class GraphRepository: IGraphRepository
    {
        private readonly IOttDb _db;

        public GraphRepository(IOttDb db)
        {
            _db = db;
        }

        public Graph Load() => FixAdjacentNodes(_db.Find<Graph>().First());

        private Graph FixAdjacentNodes(Graph graph)
        {
            if (graph == null)
            {
                throw new ArgumentNullException(nameof(graph));
            }

            foreach (var node in graph.Nodes)
            {
                node.Value.SetAdjacentNodes(
                    node.Value
                        .AdjacentNodes
                        .ToList()
                            .Select(
                                adjacmentNode => new AdjacentNode(
                                    graph.GetNode(adjacmentNode.NodeId)))
                            .ToList()
                        );
            }

            return graph;
        }

        public void Save(Graph graph)
        {
            _db.DeleteMany<Graph>();
            _db.Insert(graph);
        }
    }
}
