using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace OTT.SharedKernel.Dto
{
    [DataContract]
    [XmlType("node")]
    public class NodeDto
    {
        [DataMember]
        [XmlElement("id")]
        public string Id { get; set; }

        [DataMember]
        [XmlElement("label")]
        public string Label { get; set; }

        [DataMember]
        [XmlElement("adjacentNodes")]
        public Adjacency AdjacentNodes { get; set; }

        public NodeDto()
        {
            AdjacentNodes= new Adjacency();
        }


        [DataContract]
        public class Adjacency
        {
            [DataMember]
            [XmlElement("id")]
            public string[] NodesIds { get; set; }

            public Adjacency()
            {
                NodesIds = new List<string>().ToArray();
            }
        }
    }
}