﻿using System.Runtime.Serialization;

namespace OTT.SharedKernel.Dto
{
    [DataContract]
    public class GraphDto
    {
        public GraphDto(NodeDto[] nodes)
        {
            Nodes = nodes;
        }

        public GraphDto()
            : this(new NodeDto[0])
        {
        }

        [DataMember]
        public NodeDto[] Nodes { get; private set; }
    }
}
