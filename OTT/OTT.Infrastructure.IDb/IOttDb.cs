﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace OTT.Infrastructure.Db
{
    public interface IOttDb
    {
        void Insert<T>(T log) where T : class;

        List<T> Find<T>(Expression<Func<T, bool>> filter = null) where T : class;

        void DeleteMany<T>(Expression<Func<T, bool>> filter = null) where T : class;
    }
}
