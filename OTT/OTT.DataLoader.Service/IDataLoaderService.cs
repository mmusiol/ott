﻿using System.ServiceModel;
using OTT.SharedKernel;
using OTT.SharedKernel.Dto;

namespace OTT.DataLoader.Service
{    
    [ServiceContract]
    public interface IDataLoaderService
    {

        [OperationContract]
        RequestResult SaveGraph(GraphDto graph);        
    }    
}
