﻿using System;
using Microsoft.Practices.Unity;
using OTT.IoC;
using OTT.PathFinder;
using OTT.PathFinder.Factories;
using OTT.SharedKernel;
using OTT.SharedKernel.Dto;

namespace OTT.DataLoader.Service
{
    public class DataLoaderService : IDataLoaderService
    {
        private readonly IUnityContainer _serviceLocator = ContainerFactory.Create();
        private readonly IGraphRepository _repository;

        public DataLoaderService()
        {
            _repository = _serviceLocator.Resolve<IGraphRepository>();
        }

        public RequestResult SaveGraph(GraphDto graph)
        {     
            try
            {
                _repository.Save(GraphFactory.Create(graph));
                return new RequestResult();
            }
            catch(Exception e)
            {
                return new RequestResult(e.Message);
            }
        }        
    }
}
