﻿var OTTLoader = function(selector, options) {

    var defaults = {
        fadeOutTime: 300,
        messageSelector: ".loader-message"
    };

    $.extend(true, defaults, options);

    var instance = {
        selector: selector,
        $: $(selector),
        message: {
            $: $(selector).find(defaults.messageSelector)
        }
    };

    instance.Show = function (message) {
        instance.message.$.html(message);
        instance.$.show();
    };

    instance.Hide = function() {
        instance.$.fadeOut(defaults.fadeOutTime);
    };

    return instance;
};