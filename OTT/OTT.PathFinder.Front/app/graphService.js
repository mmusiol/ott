﻿var GraphService = function (serviceUri, loader, options) {

    var defaults = {
        warn: function (message) { toastr.warning(message); },
        messages: {
            callError: "Errow while connecting server. Please try later."
        }
    };

    $.extend(true, defaults, options);

    var instance = {
        loader: loader,
        warn: function (message) { toastr.warning(message); }
    };

    instance.uris = {
        service: serviceUri
    }
    instance.uris.graph = function () { return instance.uris.service + "graph"; }

    instance.uris.path = function (srcNode, dstNode) { return instance.uris.service + "graph/path/" + srcNode + "/" + dstNode; }

    var call = function (url, onSuccess) {
        instance.loader.Show();
        return $.ajax({
            dataType: "json",
            type: "GET",
            url: url,
            success: onSuccess,
            complete: instance.loader.Hide,
            error: function() {
                instance.warn(defaults.messages.callError);
            }

        });
    }

    instance.GetGraph = function (onSuccess) {
        return call(instance.uris.graph(), onSuccess);
    }

    instance.FindPath = function (onSuccess, srcNode, dstNode) {
        call(instance.uris.path(srcNode, dstNode), onSuccess);
    }

    return instance;
};