﻿var OTTGraph = function (containerId, findPathServiceFunc, options) {

    var requestStatus = {
        Ok: { value: 1 },
        Error: { value: 2 }
    };

    var defaults = {
        style : [
        {
            selector: "node",
            style: {
                shape: "elipse",
                'background-color': "gray",
                label: "data(label)"
            }
        },
        {
            selector: ".selected",
            style: {
                shape: "elipse",
                'background-color': "red",
                "line-color": "red",
                label: "data(label)"
            }
        }
        ],
        layout: "random",
        warn: function (message) { toastr.warning(message); },
        messages: {
            strictlyTwoNodeSelection: "Strictly two nodes must be selected!"
        }
    };

    $.extend(true, defaults, options);

    var instance = {
        containerId: containerId,
        findPathServiceFunc: findPathServiceFunc,
        Warn: defaults.warn
    };

    instance.cy = cytoscape({
        container: document.getElementById(containerId),
        style: defaults.style
    });

    instance.Reorganize = function (layout) {
        if (typeof layout !== "string") {
            layout = defaults.layout;
        }
        instance.cy.layout({ name: layout });
    };

    var validateRequestData = function (data) {
        if (data.Status === requestStatus.Ok.value) {
            return true;
        } else {
            instance.Warn(data.Message);
            return false;
        }
    };

    instance.Draw = function (data) {
        if (validateRequestData(data.GraphResult)) {

            data.GraphResult.Value.Nodes.forEach(function(node) {
                instance.cy.add({ data: { id: node.Id, label: node.Label } });
            });

            data.GraphResult.Value.Edges.forEach(function (edge) {
                instance.cy.add({ data: { id: edge.NodeA + edge.NodeB, source: edge.NodeA, target: edge.NodeB } });
            });

            instance.Reorganize();
        }
    };

    instance.ClearPath = function () {
        instance.cy.elements("edge.selected").forEach(function (edge) {
            instance.cy.$("#" + edge.id()).removeClass("selected");
        });
    };

    instance.cy.on("click", "node", function (evt) {
        instance.cy.$("#" + evt.cyTarget.id()).toggleClass("selected");
        instance.ClearPath();
    });

    instance.ShowNodesSelectionAlert = function() {
        $("#nodes-selection-alert").alert();
    }

    instance.SelectedNodes = function () {
        var selected = [];
        instance.cy.elements("node.selected").forEach(function (node) {
            selected.push(node.id());
        });
        return selected;
    };

    instance.ParsePath = function (data, onSuccess) {
        var edges = [];
        for (i = 0; i < data.Nodes.length - 1; i++) {
            edges.push(data.Nodes[i] + data.Nodes[i + 1]);
            edges.push(data.Nodes[i+1] + data.Nodes[i]);

        }
        onSuccess(edges);
    };

    instance.DrawPath = function (path) {
        path.forEach(function (edge) {
            instance.cy.$("#" + edge)[0].addClass("selected");
        });
        
    };

    instance.onPathFinded = function (data) {
        console.log(data);
        if (validateRequestData(data)) {
            instance.ParsePath(data.Value, instance.DrawPath);
        }
    };

    instance.FindPath = function() {
        var selected = instance.SelectedNodes();
        if (selected.length !== 2) {
            instance.Warn(defaults.messages.strictlyTwoNodeSelection);
        } else {
            instance.findPathServiceFunc(instance.onPathFinded, selected[0], selected[1]);
        }
    }

    return instance;
};