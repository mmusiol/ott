﻿using System;
using System.IO;
using OTT.SharedKernel.Dto;

namespace OTT.DataLoader.Interfaces
{
    public interface IDataLoader
    {
        GraphDto LoadGraph(string path = "", string filesFilter = "*.xml", Func<FileStream, NodeDto> deserializeFunc = null);
    }
}
