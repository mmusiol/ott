using System;
using System.IO;
using System.Xml.Serialization;
using OTT.SharedKernel.Dto;
using OTT.DataLoader.Exceptions;

namespace OTT.DataLoader
{
    public static class NodeDtoSerializer
    {
        public static NodeDto Deserialize(FileStream stream)
        {
            try
            {
                return (NodeDto)new XmlSerializer(typeof(NodeDto)).Deserialize(stream);
            }
            catch (InvalidOperationException e)
            {
                throw new NodeSyntaxException(e);
            }
        }
    }
}