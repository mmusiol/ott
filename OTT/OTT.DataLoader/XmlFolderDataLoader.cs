using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using OTT.DataLoader.Exceptions;
using OTT.SharedKernel.Dto;
using OTT.DataLoader.Interfaces;

namespace OTT.DataLoader
{
    public class XmlFolderDataLoader : IDataLoader
    {
        public GraphDto LoadGraph(string path = "", string filesFilter = "*.xml", Func<FileStream, NodeDto> deserializeFunc = null)
        {
            path = string.IsNullOrWhiteSpace(path) ? Directory.GetCurrentDirectory() : path;
            deserializeFunc = deserializeFunc ?? NodeDtoSerializer.Deserialize;
            return new GraphDto(GetNodes(GetXmlFiles(path, filesFilter), deserializeFunc));
        }

        private static NodeDto[] GetNodes(IEnumerable<string> xmlFiles, Func<FileStream, NodeDto> deserializeFunc) => xmlFiles.AsParallel().Select(c => CreateNodeFromXmlFile(c, deserializeFunc)).ToArray();

        private static IEnumerable<string> GetXmlFiles(string path, string filesFilter)
        {
            try
            {
                return Directory.GetFiles(path, filesFilter).ToArray();
            }
            catch (Exception e)
            {
                throw new DirectoryReadException(e);
            }
        }

        private static NodeDto CreateNodeFromXmlFile(string xmlFilePath, Func<FileStream, NodeDto> deserializeFunc)
        {
            try
            {
                NodeDto node;
                using (var readStream = new FileStream(xmlFilePath, FileMode.Open))
                {
                    node = deserializeFunc.Invoke(readStream);
                }
                return node;
            }
            catch (NodeSyntaxException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new FileAccessException(e);
            }
        }
    }
}