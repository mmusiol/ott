﻿using System;

namespace OTT.DataLoader.Exceptions
{
    public class NodeSyntaxException : DataLoaderException
    {
        public NodeSyntaxException(Exception innerException)
            : base(DataLoaderResources.NodeSyntaxException_Message, innerException)
        {
        }
    }
}