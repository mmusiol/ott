﻿using System;
using OTT.SharedKernel;

namespace OTT.DataLoader.Exceptions
{
    public abstract class DataLoaderException : OttException
    {
        protected DataLoaderException(string message)
            : base(message)
        {
        }

        protected DataLoaderException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
