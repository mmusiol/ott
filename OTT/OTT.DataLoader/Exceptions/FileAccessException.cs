﻿using System;

namespace OTT.DataLoader.Exceptions
{
    public class FileAccessException : DataLoaderException
    {
        public FileAccessException(Exception innerException)
            : base(DataLoaderResources.FileAccessException_Message, innerException)
        {
        }
    }
}