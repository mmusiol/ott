﻿using System;

namespace OTT.DataLoader.Exceptions
{
    public class DirectoryReadException : DataLoaderException
    {
        public DirectoryReadException(Exception innerException)
            : base(DataLoaderResources.DirectoryReadException_Message, innerException)
        {
        }
    }
}