﻿using System.Threading.Tasks;
using OTT.PathFinder.Exceptions;
using OTT.SharedKernel.Dto;

namespace OTT.PathFinder.Factories
{
    public static class GraphFactory
    {
        public static Graph Create(GraphDto graphDto)
        {
            var graph = new Graph();

            CreateNodes(graphDto, graph);
            CreateEdges(graphDto, graph);

            return graph;
        }

        private static void CreateNodes(GraphDto graphDto, Graph graph) => Parallel.ForEach(graphDto.Nodes, (p) =>
        {
            graph.AddNode(NodeFactory.Create(p));
        });

        private static void CreateEdges(GraphDto graphDto, Graph graph)
        {
            foreach (var node in graphDto.Nodes)
            {
                Node nodeA;
                if (!graph.Nodes.TryGetValue(node.Id, out nodeA))
                {
                    throw new NodeSemanticsException();
                }
                
                foreach (var adjacentNode in node.AdjacentNodes.NodesIds)
                {
                    Node nodeB;

                    if (!graph.Nodes.TryGetValue(adjacentNode, out nodeB))
                    {
                        throw new NodeSemanticsException();
                    }

                    nodeA.AddAdjacentNode(nodeB);
                    nodeB.AddAdjacentNode(nodeA);
                }
            }
        }
    }
}
