﻿using OTT.SharedKernel.Dto;

namespace OTT.PathFinder.Factories
{
    public static class NodeFactory
    {
        public static Node Create(NodeDto nodeDto)
        {
            var node = new Node(nodeDto.Id)
            {
                Label = nodeDto.Label,
            };

            return node;
        }
    }
}