﻿using System.Collections.Generic;

namespace OTT.PathFinder.PathFinders
{
    internal interface IPathFinder
    {
        List<Node> FindShortestPath(string srcNodeId, string dstNodeId);
    }
}
