namespace OTT.PathFinder.PathFinders.Dijkstra.Parameters
{
    internal class DijkstraAdjacentNodeParameters
    {
        public decimal EdgeWeight { get; set; }

        public DijkstraAdjacentNodeParameters(decimal edgeWeight = 1)
        {
            EdgeWeight = edgeWeight;
        }
    }
}