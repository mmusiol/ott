namespace OTT.PathFinder.PathFinders.Dijkstra.Parameters
{
    internal class DijkstraNodeParameters
    {
        public decimal DistanceFromSrcNode { get; set; }

        public bool Visited { get; set; }

        public DijkstraNodeParameters(decimal distanceFromSrcNode = decimal.MaxValue, bool visited = false)
        {
            DistanceFromSrcNode = distanceFromSrcNode;
            Visited = visited;
        }
    }
}