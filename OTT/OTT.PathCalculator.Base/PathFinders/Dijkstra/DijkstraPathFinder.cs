﻿using System;
using System.Collections.Generic;
using OTT.PathFinder.Exceptions;
using OTT.PathFinder.PathFinders.Dijkstra.Exstension;
using OTT.PathFinder.PathFinders.Dijkstra.Parameters;

namespace OTT.PathFinder.PathFinders.Dijkstra
{
    internal partial class DijkstraPathFinder: IPathFinder
    {
        private readonly Graph _graph;
        
        public DijkstraPathFinder(
            Graph graph, 
            Func<DijkstraNodeParameters> nodeParametersFunc, 
            Func<DijkstraAdjacentNodeParameters> adjacentNodeParametersFunc)
        {
            _graph = graph;
            SetUp(nodeParametersFunc, adjacentNodeParametersFunc);
        }

        private void SetUp(
            Func<DijkstraNodeParameters> nodeParametersFunc, 
            Func<DijkstraAdjacentNodeParameters> adjacentNodeParametersFunc)
        {
            _graph.InitializeParameters(nodeParametersFunc);
            _graph.InitializeAdjacentNodeParameters(adjacentNodeParametersFunc);
        }

        private void CleanUp()
        {
            _graph.CleanUpParameters();
            _graph.CleanUpAdjacentNodeParameters();
        }

        public List<Node> FindShortestPath(string srcNodeId, string dstNodeId)
        {
            var srcNode = _graph.GetNode(srcNodeId);
            srcNode.GetParameters().DistanceFromSrcNode = 0;

            if (srcNodeId == dstNodeId)
            {
                return new List<Node>() { srcNode };
            }

            TraverseGraph(srcNode);

            var dstNode = _graph.GetNode(dstNodeId);
            if (!dstNode.GetParameters().Visited)
            {
                throw new PathBetweenNodesDoNotExistsException();
            }

            var shortestPath = RetrivePath(dstNode);
            CleanUp();
            return shortestPath;
        }
    }
}
