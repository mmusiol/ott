using System;
using OTT.PathFinder.PathFinders.Dijkstra.Parameters;

namespace OTT.PathFinder.PathFinders.Dijkstra.Exstension
{
    internal static class DijkstraNodeParametersExtensions
    {
        public static DijkstraNodeParameters GetParameters(this Node node) => (DijkstraNodeParameters)node.Parameters;

        public static void InitializeParameters(this Graph graph, Func<DijkstraNodeParameters> parametersFunc) => graph.SetParameters(parametersFunc);

        public static void CleanUpParameters(this Graph graph) => graph.SetParameters(() => null);

        private static void SetParameters(this Graph graph, Func<DijkstraNodeParameters> parametersFunc)
        {
            foreach (var node in graph.Nodes)
            {
                node.Value.Parameters = parametersFunc.Invoke();
            }
        }
    }
}