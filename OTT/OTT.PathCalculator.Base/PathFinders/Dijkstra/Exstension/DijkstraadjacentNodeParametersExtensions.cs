using System;
using OTT.PathFinder.PathFinders.Dijkstra.Parameters;

namespace OTT.PathFinder.PathFinders.Dijkstra.Exstension
{
    internal static class DijkstraadjacentNodeParametersExtensions
    {
        public static DijkstraAdjacentNodeParameters GetParameters(this AdjacentNode adjacentNode) => (DijkstraAdjacentNodeParameters)adjacentNode.Parameters;

        public static void InitializeAdjacentNodeParameters(this Graph graph, Func<DijkstraAdjacentNodeParameters> parametersFunc) => graph.SetAdjacentNodeParameters(parametersFunc);

        public static void CleanUpAdjacentNodeParameters(this Graph graph) => graph.SetAdjacentNodeParameters(() => null);

        private static void SetAdjacentNodeParameters(this Graph graph, Func<DijkstraAdjacentNodeParameters> adjacentNodeParametersFunc)
        {
            foreach (var node in graph.Nodes)
            {
                foreach (var adjacentNode in node.Value.AdjacentNodes)
                {
                    adjacentNode.Parameters = adjacentNodeParametersFunc.Invoke();
                }
            }
        }
    }
}