﻿using System.Collections.Generic;
using System.Linq;
using OTT.PathFinder.PathFinders.Dijkstra.Exstension;

namespace OTT.PathFinder.PathFinders.Dijkstra
{
    internal partial class DijkstraPathFinder
    {
        private static void TraverseGraph(Node srcNode)
        {
            var processingNode = srcNode;
            var queque = new Queue<Node>();

            while (true)
            {
                processingNode.GetParameters().Visited = true;

                foreach (var adjacentNode in processingNode.AdjacentNodes.OrderBy(p => p.GetParameters().EdgeWeight))
                {
                    var distance = processingNode.GetParameters().DistanceFromSrcNode + adjacentNode.GetParameters().EdgeWeight;
                    if (adjacentNode.Node.GetParameters().DistanceFromSrcNode > distance)
                    {
                        adjacentNode.Node.GetParameters().DistanceFromSrcNode = distance;
                    }

                    if (!adjacentNode.Node.GetParameters().Visited)
                    {
                        queque.Enqueue(adjacentNode.Node);
                    }
                }

                if (!queque.Any())
                {
                    break;
                }
                processingNode = queque.Dequeue();
            }
        }
    }
}
