﻿using System.Collections.Generic;
using System.Linq;
using OTT.PathFinder.PathFinders.Dijkstra.Exstension;

namespace OTT.PathFinder.PathFinders.Dijkstra
{
    internal partial class DijkstraPathFinder
    {
        private static List<Node> RetrivePath(Node dstNode)
        {
            var processingNode = dstNode;
            var queque = new Queue<Node>();

            while (true)
            {
                queque.Enqueue(processingNode);
                var nextNode =
                    processingNode.
                        AdjacentNodes.
                        FirstOrDefault(p => p.Node.GetParameters().DistanceFromSrcNode == processingNode.GetParameters().DistanceFromSrcNode - p.GetParameters().EdgeWeight)
                        ?.Node;

                // ReSharper disable once PossibleUnintendedReferenceComparison
                if (nextNode == default(Node))
                {
                    break;
                }
                processingNode = nextNode;
            }

            return queque.ToList();
        }
    }
}
