﻿using System;
using OTT.SharedKernel;

namespace OTT.PathFinder.Exceptions
{
    public abstract class GraphException : OttException
    {
        protected GraphException(string message) 
            : base(message)
        {
        }

        protected GraphException(string message, Exception innerException) 
            : base(message, innerException)
        {
        }
    }
}
