﻿using System;

namespace OTT.PathFinder.Exceptions
{
    public class PathBetweenNodesDoNotExistsException : GraphException
    {
        public PathBetweenNodesDoNotExistsException(Exception innerException) 
            : base(PathFinderResources.PathBetweenNodesNotExistsException_Message, innerException)
        {
        }

        public PathBetweenNodesDoNotExistsException() 
            : this(null)
        {
        }
    }
}