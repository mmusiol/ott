using System;

namespace OTT.PathFinder.Exceptions
{
    public class NodeSemanticsException : GraphException
    {
        public NodeSemanticsException(Exception innerException) 
            : base(PathFinderResources.NodeSemanticsException_Message, innerException)
        {
        }

        public NodeSemanticsException() 
            : this(null)
        {
        }
    }
}