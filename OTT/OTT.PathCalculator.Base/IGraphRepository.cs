﻿namespace OTT.PathFinder
{
    public interface IGraphRepository
    {
        Graph Load();

        void Save(Graph graph);
    }
}
