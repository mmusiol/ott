using OTT.SharedKernel;

namespace OTT.PathFinder
{
    public class AdjacentNode : ValueObject<AdjacentNode>
    {
        public Node Node { get; }

        //persistence
        public string NodeId { get; set; }

        public object Parameters { get; set; }

        public AdjacentNode(Node node)
        {
            Node = node;
            NodeId = node.Id;
            Parameters = null;
        }
    }
}