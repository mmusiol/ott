﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using OTT.PathFinder.PathFinders;
using OTT.SharedKernel;

namespace OTT.PathFinder
{
   public class Graph : Entity<Guid>
    {
        public Graph(Guid id)
            : base(id)
        {
            Nodes = new ConcurrentDictionary<string, Node>();
        }

        public Graph()
            : this(Guid.NewGuid())
        {
        }

        public ConcurrentDictionary<string, Node> Nodes { get; protected set; }

        public void AddNode(Node node) => Nodes.TryAdd(node.Id, node);

       public Node GetNode(string nodeId)
        {
            Node node;
            if (!Nodes.TryGetValue(nodeId, out node))
            {
                throw new ArgumentOutOfRangeException(nameof(nodeId));
            }
            return node;
        }
        
        private IPathFinder SelectPatchFinder() => 
            new PathFinders.Dijkstra.DijkstraPathFinder(this, 
                () => new PathFinders.Dijkstra.Parameters.DijkstraNodeParameters(), 
                () => new PathFinders.Dijkstra.Parameters.DijkstraAdjacentNodeParameters());

        public List<Node> FindShortestPath(string srcNodeId, string dstNodeId) => 
            SelectPatchFinder()
            .FindShortestPath(srcNodeId, dstNodeId);
    }    
}