﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using OTT.SharedKernel;

namespace OTT.PathFinder
{
    public class Node : Entity<string>
    {
        public Node(string id) : base(id)
        {
            AdjacentNodes = new ConcurrentBag<AdjacentNode>();
        }

        public string Label { get; set; }

        public ConcurrentBag<AdjacentNode> AdjacentNodes { get; protected set; }

        public object Parameters { get; set; }

        public void AddAdjacentNode(Node node)
        {
            var adjacentNode = new AdjacentNode(node);
            if (!AdjacentNodes.Contains(adjacentNode))
            {
                AdjacentNodes.Add(adjacentNode);
            }
        }

        public void SetAdjacentNodes(List<AdjacentNode> adjacentNodes) => AdjacentNodes = new ConcurrentBag<AdjacentNode>(adjacentNodes);
    }
}