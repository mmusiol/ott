﻿using System.Configuration;
using Microsoft.Practices.Unity;
using OTT.DataLoader;
using OTT.DataLoader.Interfaces;
using OTT.Infrastructure.Db;
using OTT.Infrastructure.MongoDb;
using OTT.PathFinder;
using OTT.PathFinder.Data;

namespace OTT.IoC
{
    public static class ContainerFactory
    {
        public static IUnityContainer Create()
        {
            var container = new UnityContainer();

            container.RegisterInstance<IOttDb>(new MongoDbAdapter("OTT", ConfigurationManager.ConnectionStrings["ott"].ConnectionString));
            container.RegisterType<IDataLoader, XmlFolderDataLoader>();
            container.RegisterType<IGraphRepository, GraphRepository>(new InjectionConstructor(container.Resolve<IOttDb>()));
            
            return container;
        }
    }
}
