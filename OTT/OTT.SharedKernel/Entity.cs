﻿using System;

namespace OTT.SharedKernel
{
    public abstract class Entity<TId> : IEquatable<Entity<TId>>
    {
        public TId Id { get; protected set; }

        protected Entity()
        {
        }

        protected Entity(TId id)
        {
            if (object.Equals(id, default(TId)))
            {
                throw new ArgumentException(nameof(id));
            }

            Id = id;
        }

        public override bool Equals(object otherObject)
        {
            var entity = otherObject as Entity<TId>;
            // ReSharper disable once BaseObjectEqualsIsObjectEquals
            return entity != null ? Equals(entity) : base.Equals(otherObject);
        }

        public override int GetHashCode()
        {
            // ReSharper disable once NonReadonlyMemberInGetHashCode
            return Id.GetHashCode();
        }

        public bool Equals(Entity<TId> other)
        {
            return other != null && Id.Equals(other.Id);
        }
    }
}
