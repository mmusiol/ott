using System.Runtime.Serialization;

namespace OTT.SharedKernel
{
    [DataContract]
    public class RequestResult<T> : RequestResult where T: class 
    {
        [DataMember]
        public T Value { get; set; }

        public RequestResult(T value)
        {
            Value = value;
        }

        public RequestResult(string message)
            : base(message)
        {
        }
    }
}