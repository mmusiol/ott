﻿using System;

namespace OTT.SharedKernel
{
    public abstract class OttException : Exception
    {
        protected OttException(string message)
            : base(message)
        {
        }

        protected OttException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
