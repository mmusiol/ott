﻿using System.Runtime.Serialization;

namespace OTT.SharedKernel
{
    [DataContract]
    public class RequestResult
    {
        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public StatusType Status { get; set; }

        public RequestResult()
        {
            Status = StatusType.Ok;
        }

        public RequestResult(string message)
        {
            Status = StatusType.Error;
            Message = message;

        }

        [DataContract]
        public enum StatusType
        {
            [DataMember]
            Ok = 1,

            [DataMember]
            Error
        }
    }
}