﻿using System;
using System.Configuration;
using Microsoft.Practices.Unity;
using OTT.DataLoader.Front.DataLoaderService;
using OTT.SharedKernel;
using OTT.DataLoader.Interfaces;
using OTT.IoC;

namespace OTT.DataLoader.Front
{
    internal class Program
    {
        public static IUnityContainer ServiceLocator = ContainerFactory.Create();

        private static void Main(string[] args)
        {
            try
            {
                Notify(DataLoaderFrontResources.DataLoadingStarted_Info);
                var dataLoader = ServiceLocator.Resolve<IDataLoader>();
                var graphDtos = dataLoader.LoadGraph(filesFilter: ConfigurationManager.AppSettings["xmlFIlesFilter"]);

                RequestResult result;
                using (var client = new DataLoaderServiceClient())
                {
                    result = client.SaveGraph(graphDtos);
                }
                
                Notify(result.Status == RequestResult.StatusType.Ok ? DataLoaderFrontResources.DataLoadingSuccess_Info : result.Message);
            }
            catch (OttException e)
            {
                Notify(e.Message);
                Notify(DataLoaderFrontResources.DataLoadingFailed_Info);
            }
            ConsoleClosingFix();
        }

        private static void Notify(string message) => Console.WriteLine(message);

        private static void ConsoleClosingFix()
        {
            Notify(DataLoaderFrontResources.PressAnyKeyToExit_Message);
            Console.ReadKey();
        }
    }
}
