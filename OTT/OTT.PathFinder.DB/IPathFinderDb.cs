namespace OTT.PathFinder.DB
{
    public interface IPathFinderDb
    {
        Graph Load();        

        void Add(Graph graph);
    }
}