﻿using MongoDB.Driver;

namespace OTT.PathFinder.DB.MongoDb
{
    public partial class MongoDbAdapter: IPathFinderDb
    {
        private readonly string _connectionString;

        //move to app config
        public const string DatabaseName = "OttPathFinder";

        public MongoDbAdapter(string connectionString = "")
        {
            _connectionString = connectionString;
        }

        private IMongoClient Client
            => string.IsNullOrWhiteSpace(_connectionString)
                ? new MongoClient()
                : new MongoClient(_connectionString);


        private IMongoCollection<Graph> Collection => Client.GetDatabase(DatabaseName).GetCollection<Graph>(typeof(Graph).Name);
    }
}
