﻿using System.Collections.Generic;

namespace OTT.PathFinder.Interfaces
{
    internal interface IPathFinder
    {
        List<Node> FindShortestPath(string srcNodeId, string dstNodeId);        
    }
}
