﻿using System.Collections.Generic;
using OTT.PathFinder.Service.Model;

namespace OTT.PathFinder.Service.Factories
{
    public static class GraphPathViewModelFactory
    {
        public static GraphPathViewModel Create(List<Node> path)
        {
            var viewModel = new GraphPathViewModel();

            foreach (var node in path)
            {
                viewModel.Nodes.Add(node.Id);
            }

            return viewModel;
        }
    }
}