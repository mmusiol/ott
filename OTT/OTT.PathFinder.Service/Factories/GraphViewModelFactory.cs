﻿using OTT.PathFinder.Service.Model;

namespace OTT.PathFinder.Service.Factories
{
    public static class GraphViewModelFactory
    {
        public static GraphViewModel Create(Graph graph)
        {
            var viewModel = new GraphViewModel();

            foreach (var node in graph.Nodes)
            {
                viewModel.Nodes.Add(new GraphViewModel.Node(node.Key, node.Value.Label));

                foreach (var adjacentNode in node.Value.AdjacentNodes)
                {
                    viewModel.Edges.Add(new GraphViewModel.Edge(node.Key, adjacentNode.NodeId));
                }
            }

            return viewModel;
        }
    }
}