﻿using System;
using Microsoft.Practices.Unity;
using OTT.IoC;
using OTT.PathFinder.Exceptions;
using OTT.PathFinder.Service.Factories;
using OTT.PathFinder.Service.Model;
using OTT.SharedKernel;

namespace OTT.PathFinder.Service
{
    public class PathFinderService : IPathFinderService
    {
        private readonly IUnityContainer _serviceLocator = ContainerFactory.Create();
        private readonly IGraphRepository _repository;

        public PathFinderService()
        {
            _repository = _serviceLocator.Resolve<IGraphRepository>();
        }

        public RequestResult<GraphViewModel> Graph()
        {
            try
            {
                return new RequestResult<GraphViewModel>(GraphViewModelFactory.Create(_repository.Load()));
            }
            catch (Exception e)
            {
                return new RequestResult<GraphViewModel>(e.Message);
            }
        }

        public RequestResult<GraphPathViewModel> FindPath(string srcNodeId, string dstNodeId)
        {
            try
            {
                return new RequestResult<GraphPathViewModel>(GraphPathViewModelFactory.Create(_repository.Load().FindShortestPath(srcNodeId, dstNodeId)));
            }
            catch (PathBetweenNodesDoNotExistsException e)
            {
                return new RequestResult<GraphPathViewModel>(e.Message);

            }
            catch (Exception)
            {
                return new RequestResult<GraphPathViewModel>(PathFinderServiceResources.UnexpectedException_Message);
            }
        }
    }
}
