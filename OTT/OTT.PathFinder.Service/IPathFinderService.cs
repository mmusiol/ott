﻿using System.ServiceModel;
using System.ServiceModel.Web;
using OTT.PathFinder.Service.Model;
using OTT.SharedKernel;

namespace OTT.PathFinder.Service
{
    [ServiceContract]
    public interface IPathFinderService
    {
        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "graph/")]
        RequestResult<GraphViewModel> Graph();

        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,            
            UriTemplate = "graph/path/{srcNodeId}/{dstNodeId}")]
        RequestResult<GraphPathViewModel> FindPath(string srcNodeId, string dstNodeId);
    }
}
