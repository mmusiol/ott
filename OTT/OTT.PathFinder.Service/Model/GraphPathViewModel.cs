using System.Collections.Generic;
using System.Runtime.Serialization;

namespace OTT.PathFinder.Service.Model
{
    [DataContract]
    public class GraphPathViewModel
    {
        [DataMember]
        public List<string> Nodes { get; set; }

        public GraphPathViewModel()
        {
            Nodes = new List<string>();
        }
    }
}