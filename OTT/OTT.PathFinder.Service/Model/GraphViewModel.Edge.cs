﻿using System.Runtime.Serialization;

namespace OTT.PathFinder.Service.Model
{
    public partial class GraphViewModel
    {
        [DataContract]
        public class Edge
        {
            public Edge(string nodeA, string nodeB)
            {
                NodeA = nodeA;
                NodeB = nodeB;
            }

            [DataMember]
            public string NodeA { get; set; }

            [DataMember]
            public string NodeB { get; set; }
        }
    }
}