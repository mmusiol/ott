﻿using System.Runtime.Serialization;

namespace OTT.PathFinder.Service.Model
{
    public partial class GraphViewModel
    {
        [DataContract]
        public class Node
        {
            [DataMember]
            public string Id { get; set; }

            [DataMember]
            public string Label { get; set; }

            public Node(string nodeId, string label)
            {
                Id = nodeId;
                Label = label;
            }
        }
    }
}