﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace OTT.PathFinder.Service.Model
{
    [DataContract]
    public partial class GraphViewModel
    {
        [DataMember]
        public List<Edge> Edges { get; set; }

        [DataMember]
        public List<Node> Nodes { get; set; }           

        public GraphViewModel()
        {
            Edges = new List<Edge>();
            Nodes = new List<Node>();
        }
    }    
}